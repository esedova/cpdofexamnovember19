package com.agiletestingalliance;
import static org.junit.Assert.*;
import org.junit.Test;

public class DurationTest {
    @Test
    public void testDuration() throws Exception {

        String text = new Duration().dur();
        assertEquals("Check Text", true , text.startsWith("CP-DOF is designed specifically for corporates"));
    }
}

