package com.agiletestingalliance;
import static org.junit.Assert.*;
import org.junit.Test;

public class MinMaxTest {
    @Test
    public void testMinMaxOne() throws Exception {

        int value = new MinMax().function(1,2);
        assertEquals("Compare One", 2 , value);
     
    }

    @Test
    public void testMinMaxTwo() throws Exception {

        int value = new MinMax().function(2,1);
        assertEquals("Compare Two", 2 , value);
     
    }
}
