package com.agiletestingalliance;
import static org.junit.Assert.*;
import org.junit.Test;

public class AboutCPDOFTest {
    @Test
    public void testAboutCPDOF() throws Exception {

        String text = new AboutCPDOF().desc();
        assertEquals("Check Text", true , text.startsWith("CP-DOF certification program covers end to end DevOps Life Cycle"));
    }
}
