package com.agiletestingalliance;
import static org.junit.Assert.*;
import org.junit.Test;

public class UsefulnessTest {
    @Test
    public void testUsefulness() throws Exception {

        String text = new Usefulness().desc();
	assertEquals("Check Text", true , text.startsWith("DevOps is about transformation, about building quality in"));
    }
}
